/* 
 * This file is a nix expression which can be used to get an isolated
 * development environemt.
 *
 * When the nix package manager is installed run 
 *  > nix-shell
 * to get a shell with the dependenies of veriT present. This was only tested
 * on NixOS, but should work on other platforms which are supported by the Nix
 * packagemanger (such as MacOS X) too.
 */

{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "verit-schedgen";

  hardeningDisable = [ "all" ];
  buildInputs = with pkgs; [
      python3
      python39Packages.pandas
      python39Packages.jinja2
      python39Packages.pulp
      python39Packages.numpy
      python39Packages.setuptools
      python39Packages.black
      python39Packages.bootstrapped-pip
      python39Packages.virtualenv
      python39Packages.tkinter
      cbc
      parallel ];

}
