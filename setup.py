#!/usr/bin/env python
# -*- coding: utf-8 -*-

import io
import os

from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))

# Import the README and use it as the long-description.
short_description = (
    "Schedule generation for theorem proves based on integer programming"
)
try:
    with io.open(os.path.join(here, "README"), encoding="utf-8") as f:
        long_description = "\n" + f.read()
except FileNotFoundError:
    long_description = short_description

setup(
    name="veriT-schedgen",
    version="0.1.0",
    description=short_description,
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Hans-Jörg Schurr",
    author_email="hans-jorg.schurr@inria.fr",
    python_requires=">=3.6.0",
    url="https://gitlab.inria.fr/hschurr/verit-schedgen",
    install_requires=["pandas", "pulp", "numpy", "jinja2"],
    include_package_data=True,
    packages=["schedgen"],
    scripts=[
        "schedgen/schedgen-optimize.py",
        "schedgen/schedgen-finalize.py",
        "schedgen/schedgen-simulate.py",
        "schedgen/schedgen-visualize.py",
        "schedgen/schedgen-query.py"
    ],
    license="BSD 3-Clause License",
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
    ],
)
