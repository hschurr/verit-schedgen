import unittest
import pandas as pd
from pathlib import Path
import os

from schedgen.parsers.gridtpt import GridTPT
from schedgen.parsers.csvresult import CSVResult

# Path of the test data
test_data = Path(os.path.abspath(__file__)).parent.parent / "test_data"


def expected_frame(filtered=False):
    e = {
        "-test3": {
            "UF/a": float("inf"),
            "UF/b": 1.0,
            "UF/c": float("inf"),
            "UF/d": float("inf"),
            "UF/e": 0.9,
        },
        "-test2": {
            "UF/a": float("inf"),
            "UF/b": 5.0,
            "UF/c": 5.0,
            "UF/d": float("inf"),
            "UF/e": float("inf"),
        },
        "-test1": {
            "UF/a": 1.0,
            "UF/b": 1.0,
            "UF/c": float("inf"),
            "UF/d": float("inf"),
            "UF/e": 0.9,
        },
    }
    if not filtered:
        e["-test3"]["LRA/go"] = float("inf")
        e["-test2"]["LRA/go"] = float("inf")
        e["-test1"]["LRA/go"] = 2.0
    return pd.DataFrame.from_dict(e)


class GridTPTTestCase(unittest.TestCase):
    def test_parse_works(self):
        g = GridTPT(test_data / "gridtpt_data")
        self.assertIsNotNone(g.frame)

    def test_parse_result(self):
        g = GridTPT(test_data / "gridtpt_data")
        self.assertIsNotNone(g.frame)
        pd.testing.assert_frame_equal(g.frame, expected_frame(), check_like=True)

    def test_parse_result_simple_filter(self):
        g = GridTPT(test_data / "gridtpt_data", "UF")
        self.assertIsNotNone(g.frame)
        exp = expected_frame(filtered=True)
        pd.testing.assert_frame_equal(g.frame, exp, check_like=True)

    def test_parse_result_simple_list_filter(self):
        g = GridTPT(test_data / "gridtpt_data", ["nono", "UF"])
        self.assertIsNotNone(g.frame)
        exp = expected_frame(filtered=True)
        pd.testing.assert_frame_equal(g.frame, exp, check_like=True)

    def test_parse_result_all_filter(self):
        g = GridTPT(test_data / "gridtpt_data", "nono")
        self.assertIsNone(g.frame)


class CSVResultTestCase(unittest.TestCase):
    def test_parse_works(self):
        g = CSVResult(test_data / "csv_data.csv")
        self.assertIsNotNone(g.frame)

    def test_parse_result(self):
        g = CSVResult(test_data / "csv_data.csv")
        self.assertIsNotNone(g.frame)
        pd.testing.assert_frame_equal(g.frame, expected_frame(), check_like=True)

    def test_parse_result_simple_filter(self):
        g = CSVResult(test_data / "csv_data.csv", "UF")
        self.assertIsNotNone(g.frame)
        exp = expected_frame(filtered=True)
        pd.testing.assert_frame_equal(g.frame, exp, check_like=True)

    def test_parse_result_simple_list_filter(self):
        g = CSVResult(test_data / "csv_data.csv", ["nono", "UF"])
        self.assertIsNotNone(g.frame)
        exp = expected_frame(filtered=True)
        pd.testing.assert_frame_equal(g.frame, exp, check_like=True)

    def test_parse_result_all_filter(self):
        g = CSVResult(test_data / "csv_data.csv", "nono")
        self.assertIsNone(g.frame)
