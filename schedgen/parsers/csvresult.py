import pandas as pd
import csv


class CSVResult:
    __benchmark_column = "benchmark"
    __solved_results = ["1", "yes", "true"]
    __solved_column = "solved"
    __time_column = "time"
    __logic_column = "logic"
    __strategy_column = "strategy"

    def __init__(self, data_path, logics_filter=None):
        if logics_filter:
            self.logics_filter = list(map(lambda s: s.strip(), logics_filter))
        else:
            self.logics_filter = None

        results = {}
        with open(data_path, newline="") as csvfile:
            reader = csv.DictReader(csvfile, skipinitialspace=True, delimiter=";")
            for row in reader:
                strategy = row[self.__strategy_column].strip()
                result = row[self.__solved_column].strip().lower()
                if result not in self.__solved_results:
                    time = float("inf")
                else:
                    try:
                        time = float(row[self.__time_column].strip())
                    except ValueError:
                        time = float("inf")
                benchmark = row[self.__benchmark_column].strip()
                logic = row[self.__logic_column].strip()
                if logics_filter and logic not in logics_filter:
                    continue
                if strategy in results:
                    results[strategy][benchmark] = time
                else:
                    results[strategy] = {benchmark: time}
        frame = None
        if len(results) > 0:
            frame = pd.DataFrame.from_records(results)
            frame = frame.fillna(float("inf"))
        self.frame = frame
