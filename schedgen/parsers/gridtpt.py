import re
import pandas as pd
from schedgen.opt.helper import message


class GridTPT:
    def __init__(self, data_path, logics_filter=None):
        experiments = data_path.glob("**/*.txt")
        if isinstance(logics_filter, str):
            logics_filter = [logics_filter]
        if logics_filter:
            self.logics_filter = list(map(lambda s: s.strip(), logics_filter))
        else:
            self.logics_filter = None
        results = []
        strategies = []
        for exp in experiments:
            try:
                (exp_strategy, exp_result) = self._parse_result(exp)
                results.append(exp_result)
                strategies.append(exp_strategy)
            except ValueError as e:
                message(f"Error parsing {exp}: {e}", logics_filter)
        if len(results) > 0:
            frame = pd.DataFrame.from_records(results, index=strategies)
            frame = frame.transpose().fillna(float("inf"))
        else:
            frame = None
        self.frame = frame

    __type_re = r"^Type +: (\w+)$"
    __options_re = r"^Options +: (.*)$"
    __header_re = r"^Name +total_time +result"
    __data_re = r"^([^ ]+) +([-0-9\.]+) +([^ ]+)"

    def _parse_result(self, exp):
        results = {}
        strategy = None
        with exp.open() as f:
            for line in f:
                m = re.match(self.__data_re, line)
                if m:
                    r = m.group(3).strip()
                    if r == "1" or r == "0":
                        time = float(m.group(2))
                    else:
                        time = float("inf")  # unsolved
                    if self.logics_filter:
                        exp_logic = m.group(1).split("/")[0]
                        if exp_logic in self.logics_filter:
                            results[m.group(1)] = time
                    else:
                        results[m.group(1)] = time
                    continue
                m = re.match(self.__type_re, line)
                if m:
                    r = m.group(1).strip()
                    if not r == "perf":
                        raise ValueError("Experiment type not 'perf'.")
                    else:
                        continue
                m = re.match(self.__options_re, line)
                if m:
                    strategy = m.group(1)
                    continue

        if strategy is None:
            raise ValueError("Could not parse strategy.")
        if len(results) == 0:
            raise ValueError("Could not get any solved benchmarks.")
        return (strategy, results)
