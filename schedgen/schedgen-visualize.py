#!/usr/bin/env python3

import argparse
from schedgen.opt.helper import message, new_file_path, file_path
from schedgen.opt.schedule import read_from_file
import csv
import tkinter

args_parser = argparse.ArgumentParser(description="Visualizes a set of schedules.")

args_parser.add_argument(
    "-t",
    "--time",
    dest="time",
    metavar="T",
    default=240.0,
    type=float,
    help="Total time available to the schedules in seconds.",
)
args_parser.add_argument(
    "-a",
    "--shorthands",
    metavar="FILE",
    dest="shorthands",
    help="CSV file describing how strategies should be printed.",
    type=file_path,
)
args_parser.add_argument(
    "-p",
    "--pgf",
    dest="pgf",
    help="Write visualization as a pgf for usage in tex.",
    type=new_file_path,
)
args_parser.add_argument(
    "-c",
    dest="context",
    action="store_true",
    help="Produce a ConTeX, instead of Latex, compatible PGF file.",
)
args_parser.add_argument(
    metavar="PRE-SCHEDULE",
    dest="schedules",
    help="The pre-schedules as files.",
    nargs="*",
    type=file_path,
)


# Graphics parameters
row_width = 500
row_height = 60
row_sep = 5
padding_horizontal = 15
padding_vertical = 10
marking_space = 10
canvas_width = row_width + 2 * padding_horizontal

color_border = "#000000"
color_dark = "#434A5B"
color_background = "#FFFFFF"
color_highlight_1 = "#D4D1BE"
color_highlight_2 = "#ACBFBA"

font_cmd = "\\tiny\\sf"


def name_strategies(schedule, shorthands, max_id, names):
    for (time, s) in schedule:
        if s not in names:
            if s in shorthands:
                names[s] = (shorthands[s][0], 1)
            else:
                names[s] = (max_id, 1)
                max_id = max_id + 1
        else:
            (id, count) = names[s]
            names[s] = (id, count + 1)
    return max_id


def cut_off(total_time, schedule):
    spent_time = 0
    new_schedule = []
    for (time, strategy) in schedule:
        if spent_time + time > total_time:
            new_schedule.append(total_time - time, strategy)
            return new_schedule
        spent_time = spent_time + time
        new_schedule.append((time, strategy))
    return new_schedule


def print_strat_table(
    schedule, names, shorthands, table_width, total_time, row, output_file
):
    y = row * 0.5
    x = 0
    delta = table_width / total_time
    for (time, s) in schedule:
        highlight = s in shorthands and shorthands[s][1]
        (id, count) = names[s]
        width = time * delta
        if width < 0.5:
            node_str = ""
        else:
            node_str = f"{font_cmd} {id}"
        if not highlight:
            output_file.write(
                f"\\node[orig,minimum width={width}cm] at ({x},{y}) {{{node_str}}};\n"
            )
        else:
            output_file.write(
                f"\\node[highlight,minimum width={width}cm] at ({x},{y}) {{{node_str}}};\n"
            )
        x = x + width


def print_tiks(table_width, total_time, row, output_file):
    if total_time < 10:
        total_time = total_time * 10
    total_time = int(total_time)
    delta = table_width / total_time
    y = row * 0.5
    for i in range(0, total_time + 1):
        x = i * delta
        if i % 5 == 0:
            output_file.write(f"\\draw[line width=0.5] ({x},{y}) -- ({x},{y-0.1});\n")
        else:
            output_file.write(f"\\draw ({x},{y}) -- ({x},{y-0.07});\n")


def print_times(table_width, total_time, row, output_file):
    y = row * 0.5 - 0.23
    output_file.write(f"\\node at (0,{y}) {{{font_cmd} 0}};\n")
    output_file.write(f"\\node at ({table_width},{y}) {{{font_cmd} {total_time:g}}};\n")


def render_markings(total_time, canvas):
    orig_time = total_time
    if total_time < 10:
        total_time = total_time * 10
    total_time = int(total_time)
    delta = row_width / total_time
    offset = padding_vertical + marking_space
    for i in range(0, total_time + 1):
        x = padding_horizontal + i * delta
        if i % 5 == 0:
            canvas.create_line(x, offset, x, offset - 5)
        else:
            canvas.create_line(x, offset, x, offset - 3)
    y = offset - 4.5
    canvas.create_text(
        padding_horizontal, y, text="0", anchor="s", font="TkMenuFont", fill=color_dark
    )
    canvas.create_text(
        padding_horizontal + row_width,
        y,
        text=f"{orig_time:g}",
        anchor="s",
        font="TkMenuFont",
        fill=color_dark,
    )


def canvas_render_schedule(schedule, names, shorthands, row, canvas):
    y = padding_vertical + marking_space + (row * (row_height + row_sep))
    x = padding_horizontal
    delta = row_width / total_time
    for (time, s) in schedule:
        highlight = s in shorthands and shorthands[s][1]
        (id, count) = names[s]
        width = time * delta
        if width < 5:
            node_str = ""
        else:
            node_str = id
        if not highlight:
            canvas.create_rectangle(
                x,
                y,
                x + width,
                y + row_height,
                fill=color_highlight_1,
                outline=color_border,
            )
        else:
            canvas.create_rectangle(
                x,
                y,
                x + width,
                y + row_height,
                fill=color_highlight_2,
                outline=color_border,
            )
        canvas.create_text(
            x + (width / 2),
            y + (row_height / 2),
            text=node_str,
            anchor="center",
            font="TkMenuFont",
            fill=color_dark,
        )
        x = x + width


if __name__ == "__main__":
    args = args_parser.parse_args()
    pre_schedules = args.schedules
    message(f"Pre-schedules: {list(map(str, pre_schedules))}", [])
    total_time = args.time
    message(f"Total-time: {total_time}", [])

    # schedules are a list of (time, strategy) tuples
    schedules = [read_from_file(f) for f in pre_schedules]

    shorthands = {}
    if args.shorthands:
        with open(args.shorthands) as openfile:
            reader = csv.DictReader(openfile, delimiter=";")
            for row in reader:
                strategy = row["strategy"]
                shorthand = row["shorthand"]
                highlight = row["highlight"] in ["yes", "true", "1"]
                shorthands[strategy] = (shorthand, highlight)

    map(lambda s: cut_off(total_time, s), schedules)

    max_id = 1
    names = {}
    for schedule in schedules:
        max_id = name_strategies(schedule, shorthands, max_id, names)

    if args.pgf:
        message(f"Writing {args.pgf}.", [])
        with open(args.pgf, "w") as output_file:
            startstr = "begin{tikzpicture}"
            stopstr = "end{tikzpicture}"
            if args.context:
                font_cmd = "\\ss\\tfxx"
                startstr = "starttikzpicture"
                stopstr = "stoptikzpicture"
            output_file.write(
                f"\\{startstr}[x=1cm, y=-1cm, node distance=0 cm,outer sep = 0pt,inner sep=0]\n"
            )
            output_file.write(
                "\\tikzstyle{orig}=[draw,rectangle,minimum width=0,minimum height=0.5cm,anchor=north west]\n"
            )
            output_file.write(
                "\\tikzstyle{highlight}=[draw,rectangle,fill=black!30,minimum width=0,minimum height=0.5cm,anchor=north west]\n"
            )
            width = 12
            row = 0
            for schedule in schedules:
                print_strat_table(
                    schedule, names, shorthands, width, total_time, row, output_file
                )
                row = row + 1
            print_tiks(width, total_time, 0, output_file)
            print_times(width, total_time, 0, output_file)
            output_file.write(f"\\{stopstr}\n")
            exit(0)

    top = tkinter.Tk()

    canvas_height = (
        marking_space
        + 2 * padding_vertical
        + len(schedules) * row_height
        + (len(schedules) - 1) * row_sep
    )

    C = tkinter.Canvas(
        top, bg=color_background, height=canvas_height, width=canvas_width
    )

    for row in range(len(schedules)):
        canvas_render_schedule(schedules[row], names, shorthands, row, C)
    render_markings(total_time, C)

    C.pack()
    top.mainloop()
