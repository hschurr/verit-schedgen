#!/usr/bin/env python3

import argparse
import jinja2

from schedgen.opt.schedule import read_from_file
from schedgen.opt.helper import message, file_or_dir_path, new_file_path, file_path

args_parser = argparse.ArgumentParser(
    description="Transforms a set of pre-schedules into a scheduling script."
)

args_parser.add_argument(
    "-l",
    "--logics",
    dest="logics",
    metavar="L",
    nargs="*",
    default=["all"],
    type=str,
    help="A list of logics.  One for for each schedule.",
)
args_parser.add_argument(
    "-s",
    "--pre-schedules",
    metavar="FILE",
    dest="schedules",
    help="The pre-schedules as files.  As many as there are logics given.",
    nargs="*",
    default=["schedule.csv"],
    type=file_path,
)
args_parser.add_argument(
    "-t",
    "--time",
    dest="time",
    metavar="T",
    default=240.0,
    type=float,
    help="Total time available in seconds.",
)
args_parser.add_argument(
    "--executable",
    metavar="EXECUTABLE",
    dest="executable",
    help="Path of the executable of the solver.  Exposed to the template.",
    default=None,
    type=str,
)
args_parser.add_argument(
    metavar="TEMPLATE",
    dest="template",
    help="Template for the schedule script.",
    type=file_path,
)
args_parser.add_argument(
    metavar="OUTPUT",
    dest="output",
    help="File name to write the generated file into.",
    type=new_file_path,
)


if __name__ == "__main__":
    args = args_parser.parse_args()
    logics = args.logics
    message(f"Logics: {logics}", logics)
    pre_schedules = args.schedules
    message(f"Pre-schedules: {pre_schedules}", logics)
    message(f"Template: {args.template}", logics)

    if len(logics) != len(pre_schedules):
        message("Error: number of logics does not match number of schedules.", logics)
        exit(1)

    # schedules are a list of (time, strategy) tuples
    schedules = [read_from_file(f) for f in pre_schedules]

    dir = args.template.parent
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(dir),
        autoescape=jinja2.select_autoescape(["html", "xml"]),
    )
    template = env.get_template(args.template.name)

    referencedSchedules = {}
    for i in range(len(logics)):
        referencedSchedules[logics[i]] = schedules[i]

    rendered = template.render(
        logics=logics,
        schedules=referencedSchedules,
        time=args.time,
        executable=args.executable,
    )
    with open(args.output, "w") as outputFile:
        outputFile.write(rendered)

    message("All done.", logics)
