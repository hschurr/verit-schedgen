#!/usr/bin/env python3

import pulp
import math
import pandas as pd

from schedgen.opt.helper import message


class OptimizeOrder:
    def __init__(self, exps):
        self.frame = exps.copy()

    # Finds a good order of strategies by greedily picking the strategy
    # that minimizes the runtime of the best-possible solver for the rest.
    def find_good_order(self, schedule):
        self.schedule = schedule
        self._fill_nan()
        optimal = []
        while len(schedule) > 0:
            best = math.inf
            cost = math.inf
            for j in range(len(schedule)):
                cost = self._calculate_cost_estimate(schedule, j)
                if cost < best:
                    best = cost
                    best_idx = j
            best_schedule = schedule.pop(best_idx)
            optimal.append(best_schedule)
            self.frame = self.frame.loc[:, self.frame.columns != best_schedule[1]]
        return optimal

    def _fill_nan(self):
        columns = list(map(lambda x: x[1], self.schedule))
        self.frame = self.frame[self.frame.columns.intersection(columns)]
        for t, s in self.schedule:
            self.frame[s].loc[self.frame[s] > t] = math.nan

    def _calculate_cost_estimate(self, schedule, i):
        time, strategy = schedule[i]
        frame = self.frame.loc[:, self.frame.columns != strategy]
        cost_estimate = frame.min(axis=1) + time
        us = self.frame[strategy]
        cost = pd.concat([us, cost_estimate], axis=1).min(axis=1).sum()
        return cost

    def find_optimal_order(self, schedule, overlord=None, threads=2, logics=None):
        problem = pulp.LpProblem("Strategy_Order", pulp.LpMinimize)

        idx_schedule = list(range(len(schedule)))

        """
        One variable storing that strategy s is at position p.
        """
        pos_l = [(s, p) for s in idx_schedule for p in idx_schedule]
        v_pos = pulp.LpVariable.dicts("pos", pos_l, cat=pulp.LpBinary)

        """
        Each schedule is at only one position.  Each position can have
        at most one schedule.
        """
        for i in idx_schedule:
            problem += pulp.lpSum(v_pos[(i, j)] for j in idx_schedule) == 1
            problem += pulp.lpSum(v_pos[(j, i)] for j in idx_schedule) == 1

        """
        One variable storing that wait to get to position i.
        """
        v_wait_pos = pulp.LpVariable.dicts("wait_pos", idx_schedule, lowBound=0)
        problem += v_wait_pos[0] == 0
        for i in range(1, len(schedule)):
            problem += (
                v_wait_pos[i - 1]
                + pulp.lpSum([schedule[j][0] * v_pos[(j, i)] for j in idx_schedule])
                == v_wait_pos[i]
            )

        """
        One variable storing that wait to get to strategy s.
        uses big-M constraints
        """
        big_M = sum(map(lambda x: x[0], schedule))
        v_wait_strat = pulp.LpVariable.dicts(
            "wait_strat", idx_schedule, lowBound=0, upBound=big_M
        )
        for i in idx_schedule:
            problem += v_wait_strat[i] <= big_M
            for j in idx_schedule:
                problem += (
                    v_wait_strat[i] >= v_wait_pos[j] - big_M + big_M * v_pos[(i, j)]
                )
                problem += (
                    v_wait_strat[i] <= v_wait_pos[j] + big_M - big_M * v_pos[(i, j)]
                )

        """
        One variable per benchmark clalculating how long it takes to solve
        that benchmark by each strategy. >= to enforce min
        """

        idx_bench = list(range(len(self.frame)))
        v_bench_cost = pulp.LpVariable.dicts(
            "bench_cost", idx_bench, lowBound=0, upBound=big_M
        )

        for bench_idx in idx_bench:
            bench = self.frame.iloc[bench_idx, :]
            problem += v_bench_cost[bench_idx] >= 0
            for strat_idx in idx_schedule:
                t, s = schedule[strat_idx]
                if bench[s] <= t:
                    problem += (
                        v_bench_cost[bench_idx] >= v_wait_strat[strat_idx] + bench[s]
                    )

        """
        Target function: ∑ v_bench_cost
        """
        problem += pulp.lpSum(v_bench_cost[b] for b in idx_bench)
        message("Added optimization function.", logics)
        if overlord:
            problem.writeLP(overlord)
            message(f"Wrote LP problem to '{overlord}'.", logics)
        message("Solving...", logics)
        problem.solve(pulp.COIN_CMD(threads=threads, msg=False))
        message(f"Problem status: {pulp.LpStatus[problem.status]}.", logics)
        result = []
        for i in idx_schedule:
            for j in idx_schedule:
                if v_pos[(j, i)].value() == 1:
                    result.append(schedule[j])
        return result
