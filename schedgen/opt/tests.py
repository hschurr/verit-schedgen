import unittest
import pandas as pd

import schedgen.opt.schedule as sch


class FrameCleanupTestCase(unittest.TestCase):
    def test_prune_one_unsolved(self):
        t = pd.DataFrame.from_records({"s": {"b": float("inf")}})
        pruned = sch.prune_unsolved(t, 100.0)
        self.assertEqual(len(pruned), 0)

    def test_prune_one_unsolved_timeout(self):
        t = pd.DataFrame.from_records({"s": {"b": 1.0}})
        pruned = sch.prune_unsolved(t, 0.5)
        self.assertEqual(len(pruned), 0)

    def test_prune_one_unsolved_timeout_ok(self):
        t = pd.DataFrame.from_records({"s": {"b": 1.0}})
        pruned = sch.prune_unsolved(t, 2.0)
        pd.testing.assert_frame_equal(t, pruned)

    def test_prune_one_unsolved_epsilon(self):
        t = pd.DataFrame.from_records({"s": {"b": 1.0}})
        pruned = sch.prune_unsolved(t, 1.0, 0.1)
        self.assertEqual(len(pruned), 0)

    def test_prune_one_unsolved_epsilon_ok(self):
        t = pd.DataFrame.from_records({"s": {"b": 1.0}})
        pruned = sch.prune_unsolved(t, 1.2, 0.1)
        pd.testing.assert_frame_equal(t, pruned)

    def test_prune_complex(self):
        t = pd.DataFrame.from_records(
            {
                "s1": {
                    "b1": 0.5,
                    "b2": 0.3,
                    "b3": 0.9,
                    "b4": 10.0,
                },
                "s2": {
                    "b1": 13.0,
                    "b2": 0.7,
                    "b3": 0.2,
                    "b4": float("inf"),
                },
            }
        )
        ref = pd.DataFrame.from_records(
            {
                "s1": {
                    "b2": 0.3,
                    "b3": 0.9,
                },
                "s2": {
                    "b2": 0.7,
                    "b3": 0.2,
                },
            }
        )
        pruned = sch.prune_unsolved(t, 1.0)
        pd.testing.assert_frame_equal(ref, pruned)

    def test_trivial(self):
        t = pd.DataFrame.from_records(
            {
                "s1": {
                    "b1": 0.5,
                    "b2": 0.3,
                    "b3": 0.9,
                    "b4": 10.0,
                },
                "s2": {
                    "b1": 13.0,
                    "b2": 0.7,
                    "b3": 0.2,
                    "b4": float("inf"),
                },
            }
        )
        ref = pd.DataFrame.from_records(
            {
                "s1": {
                    "b1": 0.5,
                    "b4": 10.0,
                },
                "s2": {
                    "b1": 13.0,
                    "b4": float("inf"),
                },
            }
        )
        pruned = sch.prune_trivial(t, [1.0, 10.0, 13.0])
        pd.testing.assert_frame_equal(ref, pruned)


class OptimizerTestCase(unittest.TestCase):
    def test_simple_superseed(self):
        t = pd.DataFrame.from_records(
            {
                "s1": {"b1": 1.0, "b2": 0.3, "b3": 20.0},
                "s2": {"b1": 0.3, "b2": 10.0, "b3": 20.0},
            }
        )
        opt = sch.OptimizeSchedule([1.0], 10.0)
        opt_res = opt.find_optimal_schedule(t)
        self.assertListEqual(opt_res, [(1.0, "s1")])

    def test_two_step(self):
        t = pd.DataFrame.from_records(
            {
                "s1": {"b1": 1.0, "b2": 10.3, "b3": 0.1},
                "s2": {"b1": 0.3, "b2": 2.0, "b3": 20.0},
            }
        )
        opt = sch.OptimizeSchedule([1.0, 2.0], 10.0)
        opt_res = opt.find_optimal_schedule(t)
        self.assertListEqual(opt_res, [(1.0, "s1"), (2.0, "s2")])

    def test_two_step_useless(self):
        t = pd.DataFrame.from_records(
            {
                "s1": {"b1": 1.0, "b2": 10.3, "b3": 0.1},
                "s2": {"b1": 0.3, "b2": 2.0, "b3": 20.0},
                "s3": {"b1": 0.0, "b2": 3.0, "b3": 20.0},
            }
        )
        opt = sch.OptimizeSchedule([1.0, 2.0], 10.0)
        opt_res = opt.find_optimal_schedule(t)
        self.assertListEqual(opt_res, [(1.0, "s1"), (2.0, "s2")])
