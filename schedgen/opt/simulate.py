#!/usr/bin/env python3

import numpy as np


def runtime(schedule, frame, timeout):
    result = simulate(schedule, frame, timeout=timeout)
    total = len(result)
    result = result.loc[result < float("inf")]
    return (len(result), total, result.sum())


def simulate(schedule, frame, timeout=None, mu=0, sigma=0, seed=None):
    exps = frame.copy()

    rng = np.random.default_rng(seed)

    sc = "solved"
    # Find a unique name
    while sc in exps.columns:
        sc = sc + "_"
    exps[sc] = float("inf")

    time = 0.0
    for strategy_time, strategy in schedule:
        jitter = rng.normal(mu, sigma, exps.shape[0])

        solved = (exps[strategy] + jitter).loc[
            (exps[strategy] + jitter) <= strategy_time
        ]

        solving_time = solved.clip(0.0) + time
        solving_time.name = sc
        time = time + strategy_time

        update_idx = exps.loc[exps[sc] == float("inf")].index.intersection(solved.index)
        exps.update(solving_time.loc[update_idx])

    if timeout:
        exps[sc].loc[exps[sc] > timeout] = float("inf")

    return exps[sc]
