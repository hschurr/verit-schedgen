import argparse
from pathlib import Path


def message(msg, logic=None):
    if logic:
        print(f"{logic}: {msg}", flush=True)
    else:
        print(msg, flush=True)


def file_or_dir_path(path):
    p = Path(path.strip())
    if p.is_dir() or p.is_file():
        return p
    else:
        raise argparse.ArgumentTypeError(f"{path} is not a valid path")


def file_path(path):
    p = Path(path.strip())
    if p.is_file():
        return p
    else:
        raise argparse.ArgumentTypeError(f"{path} is not a valid file")


def new_file_path(path):
    p = Path(path.strip())
    if p.is_file():
        raise argparse.ArgumentTypeError(f"{path} exists")
    if p.parent.is_dir():
        return p
    else:
        raise argparse.ArgumentTypeError(f"{dir} is not a valid directory")
