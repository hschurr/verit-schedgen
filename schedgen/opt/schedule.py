#!/usr/bin/env python3

import pulp
import csv

from schedgen.opt.helper import message


def read_from_file(csvfile):
    """
    Read a schedule from a CSV file.

    The file should have a header and columns are separated by ";".
    The two columns are "strategy" for the strategy to execute and "time"
    for the timeout (floating point in seconds).
    """
    schedule = []
    with open(csvfile) as openfile:
        reader = csv.DictReader(openfile, delimiter=";")
        for row in reader:
            time = float(row["time"].strip())
            strategy = row["strategy"]
            schedule.append((time, strategy))
    return schedule


def prune_unsolved(frame, timeout, epsilon=0.0):
    """
    Prune unsolved problems: problems solved by no strategy
    """
    return frame[(frame <= (timeout - epsilon)).any(axis=1)]


def prune_trivial(frame, time_slices, epsilon=0.0):
    """
    Prune trivial problems: problems solved by all strategies in the shortest
    time slice
    """
    return frame[(frame > (time_slices[0] - epsilon)).any(axis=1)]


def solving_slices(frame, benchmark, strategy, slices, epsilon=0.0):
    """
    Returns the time slice indices solving a benchmark
    """
    time = frame.iloc[benchmark, strategy]
    for i in range(len(slices)):
        if slices[i] >= (time + epsilon):
            return range(i, len(slices))
    return []


class OptimizeSchedule:
    def __init__(self, time_slices, timeout, epsilon=0.0):
        self.timeout = timeout
        self.time_slices = time_slices
        self.epsilon = epsilon
        self.time_slices.sort()

    def find_greedy_schedule(self, frame, logics=None):
        remaining_time = self.timeout
        best_score = 0
        schedule = []
        while remaining_time > self.time_slices[0]:
            for strategy, solving_times in frame.iteritems():
                for slice in self.time_slices:
                    if slice > remaining_time:
                        continue
                    solved = solving_times.loc[solving_times < (slice - self.epsilon)]
                    if len(solved) > best_score:
                        best_solved = solved
                        best_score = len(solved)
                        best = (slice, strategy)
            if best is not None:
                remaining_time = remaining_time - best[0]
                frame = frame[~frame.index.isin(best_solved.index)]
                schedule.append(best)
                best_score = 0
                best = None
            else:
                return schedule
        return schedule

    def find_optimal_schedule(self, frame, overlord=None, threads=2, logics=None):
        """
        Create LP problem
        """
        problem = pulp.LpProblem("Strategy_Selection", pulp.LpMaximize)

        idx_all_benchmarks = range(len(frame))
        idx_time_slices = range(len(self.time_slices))
        idx_exps = range(len(frame.columns))
        """
        One binary variable for every experiment: was this option picked for
        one possible time slice.
        """
        v_exp = pulp.LpVariable.dicts("exp", idx_exps, cat=pulp.LpBinary)

        """
        One binary variable for every experiment, time slice pair: was this
        option picked for this time slice.
        """
        exp_ts = [(e, ts) for e in idx_exps for ts in idx_time_slices]
        v_exp_ts = pulp.LpVariable.dicts("expt", exp_ts, cat=pulp.LpBinary)

        """
        v_exp = ∑ v_exp_ts: pick exactly one time slice per experiment
        (v_exp is binary)
        """
        for e in idx_exps:
            problem += (
                pulp.lpSum(v_exp_ts[(e, ts)] for ts in idx_time_slices) == v_exp[e]
            )

        message("Added time slice constraints.", logics)

        """
        One variable per benchmark: is the benchmark solved at all?
        """
        v_b = pulp.LpVariable.dicts("b", idx_all_benchmarks, cat=pulp.LpBinary)

        """
        One variable per benchmark: how many picked experiment, time slices
        selected solve the benchmark?
        """
        v_bp = pulp.LpVariable.dicts(
            "bp", idx_all_benchmarks, cat=pulp.LpInteger, lowBound=0
        )

        message("Added benchmark variables.", logics)

        for b in idx_all_benchmarks:
            """
            v_bp = ∑ (v_exp_ts if v_exp_ts solves the benchmark)
            This includes an optimization: if a time slice solves a benchmark,
            all longers do too.
            """
            summands = []
            summands_len = 0
            for e in idx_exps:
                slices = solving_slices(frame, b, e, self.time_slices, self.epsilon)
                for ts in slices:
                    summands.append(v_exp_ts[(e, ts)])
                    summands_len = summands_len + 1
            problem += pulp.lpSum(summands) == v_bp[b]

            """ v_b * n >= v_bp: forces v_b to 1 if v_bp is at >= 1 """
            problem += v_b[b] * summands_len >= v_bp[b]
            """ v_bp + 0.5 >= v_b : forces v_b to 0 if v_bp is 0 """
            problem += v_bp[b] + 0.5 >= v_b[b]

        message("Added benchmark constraints.", logics)

        """
        Respect timeout: ∑ v_exp_ts * ts <= timeout
        """
        problem += (
            pulp.lpSum(v_exp_ts[(e, ts)] * self.time_slices[ts] for (e, ts) in exp_ts)
            <= self.timeout
        )
        message("Added timeout constraints.", logics)
        """
        Target function: ∑ v_b
        """
        problem += pulp.lpSum(v_b[b] for b in idx_all_benchmarks)
        message("Added optimization function.", logics)

        if overlord:
            problem.writeLP(overlord)
            message(f"Wrote LP problem to '{overlord}'.", logics)
        message("Solving...", logics)
        problem.solve(pulp.COIN_CMD(threads=threads, msg=False))
        message(f"Problem status: {pulp.LpStatus[problem.status]}.", logics)
        result = []
        for (e, ts) in exp_ts:
            if v_exp_ts[(e, ts)].value() == 1.0:
                rs = frame.columns[e]
                result.append((self.time_slices[ts], rs))
        return result
