#!/usr/bin/env python3

import argparse

from schedgen.opt.schedule import (
    OptimizeSchedule,
    prune_trivial,
    prune_unsolved,
    read_from_file,
)
from schedgen.opt.order import OptimizeOrder
from schedgen.opt.simulate import runtime
from schedgen.opt.helper import message, file_or_dir_path, new_file_path, file_path
from schedgen.parsers.gridtpt import GridTPT
from schedgen.parsers.csvresult import CSVResult

args_parser = argparse.ArgumentParser(
    description="Find an optimal schedule from a set of bechmarks."
)

args_parser.add_argument(
    "-l",
    "--logics",
    dest="logics",
    metavar="L",
    nargs="*",
    default=["all"],
    type=str,
    help="The logics to analyze, comma separated, 'all' for all logics.",
)
args_parser.add_argument(
    "-s",
    "--slices",
    dest="slices",
    metavar="T",
    nargs="*",
    default=[1.0, 10.0, 30.0, 60.0, 120.0, 180.0],
    type=float,
    help="Time slices to consider in seconds.",
)
args_parser.add_argument(
    "-t",
    "--time",
    dest="time",
    metavar="T",
    default=240.0,
    type=float,
    help="Total time avialable in seconds.",
)
args_parser.add_argument(
    "-d",
    "--data",
    metavar="DIR",
    required=True,
    dest="data",
    help="Location of experimental data. A folder if the GridTPT parser is selected, a CSV file if the CSV parser is selected.",
    type=file_or_dir_path,
)
args_parser.add_argument(
    "-c",
    "--cvc",
    dest="csv",
    action="store_true",
    help="Use the CSV parser instead of the GridTPT parser to read experimental data.",
)
args_parser.add_argument(
    "--greedy",
    dest="greedy",
    help="Generate a greedy schedule instead of an optimal one.",
    action="store_true",
)
args_parser.add_argument(
    "--optimal-order",
    dest="optimal_order",
    help="Calculate the optimal strategy order (expensive).",
    action="store_true",
)
args_parser.add_argument(
    "-f",
    "--full",
    dest="full",
    help="Write full script instead of CSV file (obsolet).",
    action="store_true",
)
args_parser.add_argument(
    "-z",
    dest="seconds_out",
    help="Write seconds not milliseconds (obsolet).",
    action="store_true",
)
args_parser.add_argument(
    "-p",
    "--threads",
    metavar="INT",
    dest="threads",
    help="Number of threads the LP solver can use.",
    default=2,
    type=int,
)
args_parser.add_argument(
    "-e",
    "--epsilon",
    metavar="TIME",
    dest="epsilon",
    help="Add to solving time for each benchmark before counting them as solved.",
    type=float,
    default=0.0,
)
args_parser.add_argument(
    "-o",
    "--overlord",
    metavar="FILE",
    dest="overlord",
    help="Filename to write LP problem for debugging.",
    type=new_file_path,
)
args_parser.add_argument(
    "--pre-schedule",
    metavar="FILE",
    dest="pre_schedule",
    help="CSV of schedule that is ran before the schedule to generate.",
    default=None,
    type=file_path,
)
args_parser.add_argument(
    "--pre-schedule-time",
    metavar="TIME",
    dest="pre_schedule_time",
    help="Intented time limit for the pre schedule.",
    type=float,
    default=0.0,
)
args_parser.add_argument(
    metavar="OUTPUT",
    dest="result",
    help="File to write the generated schedule to.",
    type=new_file_path,
)


def writeScript(outFile, logics, schedule, timeout, seconds=False):
    message(f"Writing schedule to {outFile}.", logics)
    with open(outFile, "w") as af:
        if len(logics) == 1:
            af.write(f"    {logics[0]})\n")
        else:
            af.write(f"    {logics})\n")
        (solved, out_of, in_time) = runtime(schedule, r.frame, timeout)
        af.write(f"        # Prediction: {solved}/{out_of} in {in_time:.2f}s\n")
        for i in range(len(schedule) - 1):
            (t, e) = schedule[i]
            if seconds:
                af.write(f"        trywith {t:.0f}\t{e}\n")
            else:
                af.write(f"        trywith {1000*t:.0f}\t{e}\n")
        (t, e) = schedule[-1]
        af.write(f"        # Calculated runtime: {t}s\n")
        af.write(f"        finishwith\t{e}\n")
        af.write("        ;;\n")


def writeCSV(outFile, logics, schedule):
    message(f"Writing CSV schedule to {outFile}.", logics)
    with open(outFile, "w") as af:
        af.write("time;strategy\n")
        for i in range(len(schedule)):
            (t, e) = schedule[i]
            af.write(f"{t:.3f};{e}\n")


def writeSchedule(outFile, logics, full, schedule, timeout, seconds=False):
    if full:
        writeScript(outFile, logics, schedule, timeout, seconds)
    else:
        writeCSV(outFile, logics, schedule)


def mergeSchedules(logics, schedule1, schedule2):
    for i in range(len(schedule1) - 1, -1, -1):
        (t1, e1) = schedule1[i]
        for j in range(len(schedule2)):
            (t2, e2) = schedule2[j]
            if e1 == e2:
                if t2 < t1:
                    message(
                        "Merging error, later scheduler has strategy for shorter timeout!",
                        logics,
                    )
                    exit(1)
                del schedule1[i]
                del schedule2[j]
                schedule1.append((t2, e2))
                return schedule1 + schedule2
    return schedule1 + schedule2


def solving_slices(frame, benchmark, strategy, slices, epsilon=0.0):
    """
    Returns the time slice indices solving a benchmark
    """
    time = frame.iloc[benchmark, strategy]
    for i in range(len(slices)):
        if slices[i] >= (time + epsilon):
            return range(i, len(slices))
    return []


def removeSolvedByPreSchedule(preSchedule, frame):
    for (t, e) in preSchedule:
        frame = frame[frame.loc[:, e] > t]
    return frame


if __name__ == "__main__":
    args = args_parser.parse_args()
    logics = args.logics
    message(f"Logics: {logics}", logics)
    timeout = args.time
    message(f"Timeout: {timeout}", logics)
    epsilon = args.epsilon
    message(f"Epsilon: {epsilon}", logics)
    time_slices = args.slices
    time_slices.sort()
    message(f"Time slices: {time_slices}", logics)
    if not args.csv:
        experiments = list(args.data.glob("**/*.txt"))
        message(f"Found {len(experiments)} .txt files.", logics)

    if "all" in logics:
        filter = None
    else:
        filter = logics

    if args.csv:
        r = CSVResult(args.data, filter)
    else:
        r = GridTPT(args.data, filter)
    exps = r.frame.copy()

    pre_schedule = None
    if args.pre_schedule:
        pre_schedule = read_from_file(args.pre_schedule)
        pre_schedule_timeout = args.pre_schedule_time
        if pre_schedule_timeout == 0.0:
            message("Error: no pre-schedule timeout given.", logics)
            exit(1)
        time_sum = sum([t[0] for t in pre_schedule])
        if time_sum > pre_schedule_timeout:
            message(
                "Error: pre-schedule has a longer runtime than the pre-schedule timeout given.",
                logics,
            )
            exit(1)
        if pre_schedule_timeout >= timeout:
            message(
                "Error: pre-schedule has a longer runtime than the given overall timeout.",
                logics,
            )
            exit(1)
        timeout = timeout - pre_schedule_timeout
        message(f"Time left after pre-schedule: {timeout}.", logics)
        exps = removeSolvedByPreSchedule(pre_schedule, exps)

    exps = prune_unsolved(exps, timeout, epsilon)
    if exps.empty:
        message("No solvable benchmarks!.", logics)
        if pre_schedule and args.result:
            message("Writing pre-schedule.", logics)
            writeSchedule(
                args.result, logics, args.full, pre_schedule, args.seconds_out
            )
            exit(0)
        exit(1)

    exps = prune_trivial(exps, time_slices, epsilon)

    schedule_optimizer = OptimizeSchedule(time_slices, timeout, epsilon)
    # Format: (time, strategy)
    if args.greedy:
        best_schedule = schedule_optimizer.find_greedy_schedule(exps, logics=logics)
    else:
        schedule = schedule_optimizer.find_optimal_schedule(
            exps, overlord=args.overlord, threads=args.threads, logics=logics
        )

        if len(schedule) == 0:
            message("  Error: got empty schedule.", logics)
            exit(1)
        if len(schedule) == 1:
            message("Have singular best step:", logics)
            t, best_opt = schedule[0]
            longest = time_slices[-1]
            message(f"  For {t:.2f}s\t'{best_opt}'", logics)
            order_optimizer = OptimizeOrder(exps)
            (best_count, _, best_time) = runtime(
                [(longest, best_opt)], r.frame, timeout
            )
            message(f"This solves {best_count} in {best_time}s.", logics)
            for e in exps:
                (new_count, _, time) = runtime([(longest, e)], r.frame, timeout)
                if new_count > best_count:
                    message(
                        "Something went wrong: optimizer returned a better schedule.",
                        logics,
                    )
                    exit(1)
                if new_count == best_count and time < best_time:
                    best_time = time
                    best_opt = e
            message("The best schedule is:", logics)
            message(f"  Run\t'{best_opt}'", logics)
            message(f"This solves {best_count} in {best_time}s.", logics)
            after_count = best_count
            after_total_time = best_time
            best_schedule = [(t, best_opt)]
        else:
            message("Solution:", logics)
            for (t, e) in schedule:
                message(f"  For {t:.2f}s\t'{e}'", logics)

            order_optimizer = OptimizeOrder(exps)

            (before_count, total_count, before_total_time) = runtime(
                schedule, r.frame, timeout
            )
            message(
                f"Current schedule solves {before_count}/{total_count} in {before_total_time:.2f}s.",
                logics,
            )

            message("Calculating best order.", logics)
            overlord = args.overlord
            if overlord:
                overlord = "order_" + overlord
            if args.optimal_order:
                best_schedule = order_optimizer.find_optimal_order(
                    schedule, overlord=overlord, threads=args.threads, logics=logics
                )
                message("\nOptimal schedule:", logics)
            else:
                best_schedule = order_optimizer.find_good_order(schedule)
                message("\nTweaked schedule:", logics)
            for (t, e) in best_schedule:
                message(f"  For {t:.2f}s\t'{e}'", logics)
            (after_count, total_count, after_total_time) = runtime(
                best_schedule, r.frame, timeout
            )
            message(
                f"This schedule solves {after_count}/{total_count} in {after_total_time:.2f}s.",
                logics,
            )

    if pre_schedule:
        message("Merging with pre-schedule.", logics)
        full_schedule = mergeSchedules(logics, pre_schedule, best_schedule)
    else:
        full_schedule = best_schedule

    if args.result:
        writeSchedule(
            args.result, logics, args.full, full_schedule, timeout, args.seconds_out
        )

    message("All done.", logics)
