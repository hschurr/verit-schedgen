#!/usr/bin/env python3

import argparse

from schedgen.opt.schedule import read_from_file
from schedgen.parsers.gridtpt import GridTPT
from schedgen.parsers.csvresult import CSVResult
from schedgen.opt.helper import file_or_dir_path, new_file_path, file_path

args_parser = argparse.ArgumentParser(
    description="Prints some performance data of a schedule."
)

args_parser.add_argument(
    "-d",
    "--data",
    metavar="DIR",
    required=True,
    dest="data",
    help="Location of experimental data. A folder if the GridTPT parser is selected, a CSV file if the CSV parser is selected.",
    type=file_or_dir_path,
)
args_parser.add_argument(
    "-c",
    "--csv",
    dest="csv",
    action="store_true",
    help="Use CSV instead of the GridTPT parser to read data.",
)
args_parser.add_argument(
    "-l",
    "--logics",
    dest="logics",
    metavar="L",
    nargs="*",
    default=["all"],
    type=str,
    help="The logics to consider. 'all' for all logics.",
)
args_parser.add_argument(
    "-t",
    "--time",
    dest="time",
    metavar="T",
    default=float("inf"),
    type=float,
    help="Total time available to the schedule in seconds.",
)
args_parser.add_argument(
    "-s",
    "--score",
    dest="score",
    action="store_true",
    help="Print scores (such as PAR-2) instead of lists.",
)
args_parser.add_argument(
    "-q",
    "--query",
    dest="query",
    metavar="QUERY",
    default="all",
    type=str,
    help="Either: schedule, best, unsolved, compare.",
)
args_parser.add_argument(
    metavar="FILE",
    dest="schedule",
    help="Pre-schedule to simulate.",
    type=file_path,
)


def clip_inf(max):
    return lambda x: max if x == float("inf") else x


if __name__ == "__main__":
    args = args_parser.parse_args()
    logics = args.logics
    timeout = args.time
    if args.score and timeout == float("inf"):
        print("Error: calculating scores needs a finite timeout.")
        exit(1)
    if not args.csv:
        experiments = list(args.data.glob("**/*.txt"))

    if "all" in logics:
        filter = None
    else:
        filter = logics

    if args.csv:
        r = CSVResult(args.data, filter)
    else:
        r = GridTPT(args.data, filter)
    exps = r.frame.copy()

    pre_schedule = None
    pre_schedule = read_from_file(args.schedule)

    # Find a unique names
    sc = "solved"
    while sc in exps.columns:
        sc = sc + "_"
    scb = "solved-best"
    while scb in exps.columns:
        scb = scb + "_"

    exps[sc] = float("inf")
    exps[scb] = float("inf")

    time = 0.0

    # Some pandas magic to calculated the simulated things
    for strategy_time, strategy in pre_schedule:
        solved = (exps[strategy]).loc[(exps[strategy]) <= strategy_time]

        solving_time = solved.clip(0.0) + time
        solving_time.name = sc
        time = time + strategy_time

        update_idx = exps.loc[exps[sc] == float("inf")].index.intersection(solved.index)
        exps.update(solving_time.loc[update_idx])

    exps[sc].loc[exps[sc] > timeout] = float("inf")

    solved_best = exps.min(axis=1)
    solved_best.loc[solved_best > timeout] = float("inf")

    # help="Either: schedule, best, compare-best, compare-all.",
    if args.query in ["all", "schedule"]:
        if args.score:
            if args.query == "all":
                print("Schedule scores:")
            print(f"Solves:       {len(exps[sc].loc[exps[sc] <= timeout])}")
            print(f"Solving time: {exps[sc].loc[exps[sc] <= timeout].sum():.2f}")
            exps[sc].loc[exps[sc] > timeout] = 2 * timeout
            print(f"PAR-2 score:  {exps[sc].sum():.2f}")
            exps[sc].loc[exps[sc] > timeout] = float("inf")
        else:
            if args.query == "all":
                print("Benchmarks solved by the schedule:")
            print(exps[sc].to_string())

    if args.query in ["all", "best"]:
        if args.score:
            if args.query == "all":
                print("\nTheoretical best solver scores:")
            print(f"Solves:       {len(solved_best.loc[solved_best <= timeout])}")
            print(f"Solving time: {solved_best.loc[solved_best <= timeout].sum():.2f}")
            solved_best.loc[solved_best > timeout] = 2 * timeout
            print(f"PAR-2 score:  {solved_best.sum():.2f}")
            solved_best.loc[solved_best > timeout] = float("inf")
        else:
            if args.query == "all":
                print("\nBenchmarks solved by the theoretical best solver:")
            print(solved_best.to_string())

    if args.query in ["all", "unsolved"]:
        if args.query == "all":
            print("\nBenchmarks not solved by the schedule:")
        for i in list(exps[sc].loc[exps[sc] == float("inf")].index):
            print(i)

    if args.query in ["all", "compare"]:
        if args.query == "all":
            print(
                "\nBenchmarks solved by the theoretical best solver, but not by the schedule:"
            )
        ls = list(
            exps[sc].loc[exps[sc] == float("inf")].loc[solved_best < float("inf")].index
        )
        for i in ls:
            print(i)
