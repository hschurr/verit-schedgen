#!/usr/bin/env python3

import argparse
import numpy as np

from datetime import datetime
from schedgen.opt.schedule import read_from_file
from schedgen.opt.helper import message, file_or_dir_path, new_file_path, file_path
from schedgen.opt.simulate import simulate
from schedgen.parsers.gridtpt import GridTPT
from schedgen.parsers.csvresult import CSVResult

args_parser = argparse.ArgumentParser(
    description="Simulate a schedule on a set of bechmarks."
)

args_parser.add_argument(
    "-d",
    "--data",
    metavar="DIR",
    required=True,
    dest="data",
    help="Location of experimental data. A folder if the GridTPT parser is selected, a CSV file if the CSV parser is selected.",
    type=file_or_dir_path,
)
args_parser.add_argument(
    "-c",
    "--csv",
    dest="csv",
    action="store_true",
    help="Use CSV instead of the GridTPT parser to read data.",
)
args_parser.add_argument(
    "-l",
    "--logics",
    dest="logics",
    metavar="L",
    nargs="*",
    default=["all"],
    type=str,
    help="The logics to use for simulation. 'all' for all logics.",
)
args_parser.add_argument(
    "-t",
    "--time",
    dest="time",
    metavar="T",
    default=None,
    type=float,
    help="Total time available to the schedule in seconds.",
)
args_parser.add_argument(
    "--mu",
    metavar="MEAN",
    dest="mu",
    help="Mean value of the normal distribution used to add jitter to solving time.",
    type=float,
    default=0.0,
)
args_parser.add_argument(
    "--sigma",
    metavar="DEVIATION",
    dest="sigma",
    help="Standard deviation of the normal distribution used to add jitter to solving time.",
    type=float,
    default=0.0,
)
args_parser.add_argument(
    "--seed",
    metavar="INT",
    dest="seed",
    help="Seed to use for the random number generator.",
    default=None,
    type=int,
)
args_parser.add_argument(
    metavar="FILE",
    dest="schedule",
    help="Pre-schedule to simulate.",
    type=file_path,
)
args_parser.add_argument(
    metavar="OUTPUT",
    dest="output",
    help="File name to write the generated file into.",
    type=new_file_path,
)


def clip_inf(max):
    return lambda x: max if x == float("inf") else x


def writeResult(args, result):
    message(f"Writing result to {args.output}.", logics)
    with open(args.output, "w") as af:
        af.write("-gridTPT report---------------\n")
        af.write(f"Date             : {datetime.now():%Y%m%d%H%M%S}\n")
        af.write("Type             : perf\n")
        af.write("Comment          : Simulated run\n")
        af.write("-informations-----------------\n")
        af.write("Hardware         : \n")
        af.write(f"Executable       : {args.schedule}\n")
        af.write("Options          : \n")
        af.write(f"Benchmarks type  : {args.logics}\n")
        if args.time:
            af.write(f"CPU limit        : {args.time:.0f}s\n")
        else:
            af.write("CPU limit        : 99999999s\n")
        af.write("-statistics-------------------\n")
        time = result.map(clip_inf(args.time)).sum()
        af.write(f"Cumulative time  : {time/60:.0f}m ({time/(60*60):.0f}h)\n")
        af.write("Total time       : 0m \n")
        af.write("Nb of clients    : 1\n")
        af.write("Opt. Sched. time : 0m \n")
        af.write("-summary----------------------\n")
        benchmarks = len(result)
        successes = len(result.loc[result < float("inf")])
        af.write(f"Total number of benchmarks    : {benchmarks}\n")
        af.write(f"Number of success             : {successes}\n")
        af.write("Number of unknown             : 0\n")
        af.write(f"Number of time out            : {benchmarks-successes}\n")
        af.write("Number of errors              : 0\n")
        af.write("-data-------------------------\n")
        padding = max(result.index.map(len).max(), len("Name"))
        af.write(f"{'Name':<{padding}} total_time result\n")
        for key in result.index:
            if result[key] == float("inf"):
                rtime = "--"
                rout = "NA"
            else:
                rtime = f"{result[key]:.2f}"
                rout = "0"
            af.write(f"{key:<{padding}} {rtime:>10} {rout:>6}\n")
        af.write("-eof--------------------------\n")


if __name__ == "__main__":
    args = args_parser.parse_args()
    logics = args.logics
    message(f"Logics: {logics}", logics)
    timeout = args.time
    message(f"Data: {args.data}", logics)
    if not args.csv:
        experiments = list(args.data.glob("**/*.txt"))
        message(f"Found {len(experiments)} .txt files.", logics)
    message(f"Timeout: {timeout}", logics)
    message(f"μ: {args.mu}", logics)
    message(f"σ: {args.sigma}", logics)

    if "all" in logics:
        filter = None
    else:
        filter = logics

    if args.csv:
        r = CSVResult(args.data, filter)
    else:
        r = GridTPT(args.data, filter)
    exps = r.frame.copy()

    pre_schedule = None
    pre_schedule = read_from_file(args.schedule)

    result = simulate(pre_schedule, exps, timeout, args.mu, args.sigma, args.seed)

    message("Writing output.", logics)
    writeResult(args, result)

    message("All done.", logics)
