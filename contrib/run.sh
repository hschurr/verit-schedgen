#!/usr/bin/env bash

SLICES_24='1 2 3 4 5 6 7 8 9 10 11 12 14 16 18 20 22 24'
PRE_OUT_24=pre_scheduler_24.sh
OUT_24=scheduler_24.sh

SLICES='1 2 3 4 5 7 10 15 20 25 30 40 45 55 60 75 90 105 120 135 150 180 210 240 270'
PRE_OUT=pre_scheduler_1200.sh
OUT=scheduler_1200.sh

DIR=./data/
LOGICS='ALIA AUFLIA AUFLIRA LIA UF UFIDL UFLIA UFLRA'
#LOGICS='UF'
TEMPLATE=scheduler_template

for l in $LOGICS
do
    rm -f result_$l
    rm -f result_24_$l
done

# NOTE: if interupted with ctrl-c the second process is not killed, this however is no problem here
echo $LOGICS | parallel --line-buffer -d ' ' "schedgen-optimize.py -e 0.1 -l {} -s $SLICES -t 1200 -d $DIR -r result_1200_{} -p 6" &
echo $LOGICS | parallel --tagstring 24 --line-buffer -d ' ' "schedgen-optimize.py -e 0.1 -l {} -s $SLICES_24 -t 24 -d $DIR -r result_24_{} -p 6" &&
fg

echo "All optimizations done, building scheduler."

cat result_1200_* > $PRE_OUT
cat result_24_* > $PRE_OUT_24

sed -e "/REPLACEME/{r $PRE_OUT" -e "d}" $TEMPLATE > $OUT
sed -e "/REPLACEME/{r $PRE_OUT_24" -e "d}" $TEMPLATE > $OUT_24

chmod +x $OUT
chmod +x $OUT_24

echo "All done."

