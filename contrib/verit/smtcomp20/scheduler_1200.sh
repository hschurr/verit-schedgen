#!/usr/bin/env bash

exec=./veriT
problem="$1"

logic=$(expr "$(grep -m1 '^[^;]*set-logic' "$problem")" : ' *(set-logic  *\([A-Z_]*\) *) *$')

function trywith {
  limit=$1; shift;
  result="$(ulimit -S -t "$limit";$exec --disable-banner --disable-print-success "$@" $problem 2>&1)"
  case "$result" in
      sat|unsat)
          echo "$result";
          exit 0;;
  esac
}

function finishwith {
    $exec --disable-banner --disable-print-success "$@" $problem 2>&1
}

case "$logic" in
    ALIA)
        # Prediction: 41/41 in 1.65
        # Calculated runtime: 15.0
        trywith 25	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 270	--ccfv-breadth --inst-deletion --index-SAT-triggers --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 5	--triggers-new --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=10000
        trywith 30	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-deletion --index-SAT-triggers
        trywith 2	--triggers-new --triggers-sel-rm-specific
        trywith 10	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 2	--index-SIG --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 90	--mdli --limit-sk-qform=9 --limit-qform=1000 --inst-sorts-threshold=100
        trywith 4	--index-SIG --triggers-new --triggers-sel-rm-specific
        trywith 45	--inst-deletion --index-SAT-triggers --ccfv-branches=100000 --CIs-bound=1
        trywith 150	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        trywith 180	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 10	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 150	--inst-deletion --index-SAT-triggers --ccfv-breadth
        trywith 75	--index-SIG --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 60	--CIs-off --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        # Calculated runtime: 90.0
        finishwith	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine
        ;;
    AUFLIA)
        # Prediction: 2426/2436 in 19491.38
        trywith 270	--ccfv-breadth --inst-deletion --index-SAT-triggers --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 5	--triggers-new --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=10000
        trywith 30	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-deletion --index-SAT-triggers
        trywith 2	--triggers-new --triggers-sel-rm-specific
        trywith 10	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 2	--index-SIG --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 90	--mdli --limit-sk-qform=9 --limit-qform=1000 --inst-sorts-threshold=100
        trywith 4	--index-SIG --triggers-new --triggers-sel-rm-specific
        trywith 45	--inst-deletion --index-SAT-triggers --ccfv-branches=100000 --CIs-bound=1
        trywith 150	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        trywith 180	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 10	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 150	--inst-deletion --index-SAT-triggers --ccfv-breadth
        trywith 75	--index-SIG --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 60	--CIs-off --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        # Calculated runtime: 90.0
        finishwith	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine
        ;;
    AUFLIRA)
        # Prediction: 19341/19341 in 162.03
        trywith 30	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 1	--index-SIG --triggers-new --triggers-sel-rm-specific
        trywith 60	--mdli --limit-sk-qform=15 --limit-qform=1000 --inst-sorts-threshold=10000
        trywith 270	--ccfv-breadth --inst-deletion --index-SAT-triggers --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 5	--triggers-new --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=10000
        trywith 30	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-deletion --index-SAT-triggers
        trywith 2	--triggers-new --triggers-sel-rm-specific
        trywith 10	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 2	--index-SIG --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 90	--mdli --limit-sk-qform=9 --limit-qform=1000 --inst-sorts-threshold=100
        trywith 4	--index-SIG --triggers-new --triggers-sel-rm-specific
        trywith 45	--inst-deletion --index-SAT-triggers --ccfv-branches=100000 --CIs-bound=1
        trywith 150	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        trywith 180	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 10	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 150	--inst-deletion --index-SAT-triggers --ccfv-breadth
        trywith 75	--index-SIG --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 60	--CIs-off --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        finishwith	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine
        ;;
    LIA)
        # Prediction: 162/162 in 0.15
        # Calculated runtime: 5.0
        trywith 25	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        trywith 1	--index-SIG --triggers-new --triggers-sel-rm-specific
        trywith 60 --mdli --limit-sk-qform=15 --limit-qform=1000 --inst-sorts-threshold=10000
        trywith 270	--ccfv-breadth --inst-deletion --index-SAT-triggers --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 5	--triggers-new --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=10000
        trywith 30	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-deletion --index-SAT-triggers
        trywith 2	--triggers-new --triggers-sel-rm-specific
        trywith 10	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 2	--index-SIG --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 90	--mdli --limit-sk-qform=9 --limit-qform=1000 --inst-sorts-threshold=100
        trywith 4	--index-SIG --triggers-new --triggers-sel-rm-specific
        trywith 45	--inst-deletion --index-SAT-triggers --ccfv-branches=100000 --CIs-bound=1
        trywith 150	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        trywith 180	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 10	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 150	--inst-deletion --index-SAT-triggers --ccfv-breadth
        trywith 75	--index-SIG --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 60	--CIs-off --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        finishwith	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine
        ;;
    UF)
        # Prediction: 3410/3434 in 48714.71
        trywith 180	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 3	--inst-deletion --index-SAT-triggers
        trywith 55	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-deletion --index-SAT-triggers --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 40	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --ccfv-breadth --inst-sorts-threshold=1000000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=10000
        trywith 25	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 90	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 135	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 90	--triggers-new --mdli --limit-sk-qform=15 --limit-qform=500 --inst-sorts-threshold=10000
        trywith 55	--inst-deletion --index-SAT-triggers --ccfv-breadth
        trywith 30	--CIs-off --triggers-new --triggers-sel-rm-specific --triggers-restrict-combine
        trywith 120	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        trywith 120	--ccfv-breadth
        trywith 150	--mdli --limit-sk-qform=9 --limit-qform=1000 --inst-sorts-threshold=100
        trywith 40	--mdli --limit-sk-qform=9 --limit-qform=1000 --inst-sorts-threshold=1000
        trywith 25	--index-SIG --triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        # Calculated runtime: 30.0
        finishwith	--index-SIG --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        ;;
    UFIDL)
        # Prediction: 55/55 in 0.09
        # Calculated runtime: 7.0
        trywith 12	--ccfv-breadth --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 180	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 3	--inst-deletion --index-SAT-triggers
        trywith 55	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-deletion --index-SAT-triggers --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 40	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --ccfv-breadth --inst-sorts-threshold=1000000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=10000
        trywith 25	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 90	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 135	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 90	--triggers-new --mdli --limit-sk-qform=15 --limit-qform=500 --inst-sorts-threshold=10000
        trywith 55	--inst-deletion --index-SAT-triggers --ccfv-breadth
        trywith 30	--CIs-off --triggers-new --triggers-sel-rm-specific --triggers-restrict-combine
        trywith 120	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        trywith 120	--ccfv-breadth
        trywith 150	--mdli --limit-sk-qform=9 --limit-qform=1000 --inst-sorts-threshold=100
        trywith 40	--mdli --limit-sk-qform=9 --limit-qform=1000 --inst-sorts-threshold=1000
        trywith 25	--index-SIG --triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        finishwith	--index-SIG --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        ;;
    UFLIA)
        # Prediction: 7611/7638 in 70044.01
        trywith 75	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 15	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-deletion --index-SAT-triggers
        trywith 7	--index-SIG --triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 10	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        trywith 75	--triggers-new --triggers-sel-rm-specific
        trywith 20	--mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 55	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-deletion --index-SAT-triggers --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 75	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 90	--index-SIG --triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 25	--inst-deletion --index-SAT-triggers --ccfv-branches=100000 --CIs-bound=1 --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 55	--CIs-off --triggers-new --triggers-sel-rm-specific --triggers-restrict-combine
        trywith 30	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 40	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 25	--index-SIG --triggers-new --triggers-sel-rm-specific
        trywith 120	--triggers-new --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=10000
        trywith 210	--ccfv-breadth --inst-deletion --index-SAT-triggers
        trywith 180	--mdli --limit-sk-qform=9 --limit-qform=1000 --inst-sorts-threshold=100
        # Calculated runtime: 90.0
        finishwith	--ccfv-breadth
        ;;
    UFLRA)
        # Prediction: 10/10 in 0.00
        # Calculated runtime: 10.0
        trywith 25	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        trywith 180	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 3	--inst-deletion --index-SAT-triggers
        trywith 55	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-deletion --index-SAT-triggers --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 40	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --ccfv-breadth --inst-sorts-threshold=1000000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=10000
        trywith 25	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 90	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 135	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 90	--triggers-new --mdli --limit-sk-qform=15 --limit-qform=500 --inst-sorts-threshold=10000
        trywith 55	--inst-deletion --index-SAT-triggers --ccfv-breadth
        trywith 30	--CIs-off --triggers-new --triggers-sel-rm-specific --triggers-restrict-combine
        trywith 120	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        trywith 120	--ccfv-breadth
        trywith 150	--mdli --limit-sk-qform=9 --limit-qform=1000 --inst-sorts-threshold=100
        trywith 40	--mdli --limit-sk-qform=9 --limit-qform=1000 --inst-sorts-threshold=1000
        trywith 25	--index-SIG --triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        # Calculated runtime: 30.0
        finishwith	--index-SIG --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        ;;
    *)
        finishwith
        ;;
esac

# Configuration script structure adapted from CVC4's configuration for SMTCOMP 2015

