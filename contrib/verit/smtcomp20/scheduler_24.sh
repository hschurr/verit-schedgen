#!/usr/bin/env bash

exec=./veriT
problem="$1"

logic=$(expr "$(grep -m1 '^[^;]*set-logic' "$problem")" : ' *(set-logic  *\([A-Z_]*\) *) *$')

function trywith {
  limit=$1; shift;
  result="$(ulimit -S -t "$limit";$exec --disable-banner --disable-print-success "$@" $problem 2>&1)"
  case "$result" in
      sat|unsat)
          echo "$result";
          exit 0;;
  esac
}

function finishwith {
    $exec --disable-banner --disable-print-success "$@" $problem 2>&1
}

case "$logic" in
    ALIA)
        # Prediction: 41/41 in 1.65
        # Calculated runtime: 4.0
        finishwith	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        ;;
    AUFLIA)
        # Prediction: 2361/2436 in 655.01
        trywith 12	--ccfv-breadth --inst-deletion --index-SAT-triggers
        trywith 1	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-deletion --index-SAT-triggers
        trywith 2	--triggers-new --triggers-sel-rm-specific
        trywith 4	--mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 1	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        trywith 2	--index-SIG --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        # Calculated runtime: 2.0
        finishwith	--inst-deletion --index-SAT-triggers --ccfv-branches=100000 --CIs-bound=1 --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        ;;
    AUFLIRA)
        # Prediction: 19341/19341 in 162.03
        trywith 8	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 1	--index-SIG --triggers-new --triggers-sel-rm-specific
        # Calculated runtime: 7.0
        finishwith	--mdli --limit-sk-qform=15 --limit-qform=1000 --inst-sorts-threshold=10000
        ;;
    LIA)
        # Prediction: 162/162 in 0.15
        # Calculated runtime: 3.0
        finishwith	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=100
        ;;
    UF)
        # Prediction: 3266/3434 in 1169.23
        trywith 5	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 2	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-deletion --index-SAT-triggers --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 1	--triggers-new --triggers-sel-rm-specific --triggers-restrict-combine
        trywith 2	--CIs-off
        trywith 4	--triggers-new --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=10000
        trywith 1	--inst-deletion --index-SAT-triggers --ccfv-branches=100000 --CIs-bound=1
        trywith 1	--triggers-new --triggers-sel-rm-specific --triggers-multi-off
        trywith 1	--index-SIG --triggers-new --triggers-sel-rm-specific
        trywith 6	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        # Calculated runtime: 1.0
        finishwith	--mdli --limit-sk-qform=15 --limit-qform=1000 --inst-sorts-threshold=10000
        ;;
    UFIDL)
        # Prediction: 55/55 in 0.09
        # Calculated runtime: 2.0
        finishwith	--ccfv-breadth --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        ;;
    UFLIA)
        # Prediction: 7405/7638 in 2098.02
        trywith 7	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 4	--index-SIG --triggers-new --triggers-sel-rm-specific --triggers-restrict-combine --inst-sorts-threshold=100000 --ematch-exp=10000000 --ccfv-index=100000 --ccfv-index-full=1000
        trywith 2	--triggers-new --mdli --limit-sk-qform=9 --limit-qform=500 --inst-sorts-threshold=10000
        trywith 4	--ccfv-breadth --mdli --limit-sk-qform=9 --limit-qform=100 --inst-sorts-threshold=100
        trywith 3	--triggers-new --triggers-sel-rm-specific --triggers-multi-off --inst-deletion --index-SAT-triggers
        # Calculated runtime: 4.0
        finishwith	--ccfv-breadth --index-fresh-sorts --inst-deletion --index-SAT-triggers
        ;;
    UFLRA)
        # Prediction: 10/10 in 0.00
        # Calculated runtime: 4.0
        finishwith	--ccfv-breadth --index-fresh-sorts --inst-deletion --index-SAT-triggers
        ;;
    *)
        finishwith
        ;;
esac

# Configuration script structure adapted from CVC4's configuration for SMTCOMP 2015

