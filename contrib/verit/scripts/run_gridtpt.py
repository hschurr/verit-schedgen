#!/usr/bin/env python3

import argparse
import math
import random
import string
import sys
import re
import os
from pathlib import Path
import math

CLUSTER = "grvingt"
NODES = 25
RESERVATION = "02:00"
TIMEOUT = 60

LOGICS = "ALIA AUFLIA AUFLIRA LIA UF UFIDL UFLIA UFLRA LRA"
# LOGICS="Green_veriT isabelle-mirabelle Ordered_Resolution_Prover_veriT"

EXPERIMENT_FOLDER = "/home/hans/Work/phd/deep-unit-processing/experiments/data"
CONFIG_TEMPLATE = "config_template"
CONFIG_TARGET = "config/GridTPT-generated.cfg"

STRATEGIE_FILE = "/home/hans/Work/phd/deep-unit-processing/experiments/strategies/strategies_under_test"
DONE_FILE = "strategies_done"

if __name__ == "__main__":
    with open(STRATEGIE_FILE) as f:
        strategies = f.readlines()
        strategies = list(map(lambda x: x.strip(), strategies))
    with open(DONE_FILE) as f:
        done = f.readlines()
        done = list(map(lambda x: x.strip(), done))
    for strategie in strategies:
        if strategie in done:
            print(f"Strategie {strategie} done, skipping.")
            continue
        os.system(
            f'sed "s/CMDLINEOPTION/{strategie}/" {CONFIG_TEMPLATE} > {CONFIG_TARGET}'
        )

        rnd = "".join(random.choices(string.ascii_uppercase + string.digits, k=12))
        name = f"res-{rnd}.txt"
        command = f'bin/GridTPT-launcher.py run -C "{CLUSTER}" -q -i nancy.g5k \
-w {RESERVATION} -n {NODES} -T "perf" -t {TIMEOUT} -b "{LOGICS}" -c \
"run" -O "{name}"'
        result = os.system(command)
        os.system("rm *.exp")
        if result != 0:
            print(f"error on strategy {strategie}")
            continue
        result = os.system(
            f'scp "nancy_g5k:/home/hschurr/{name}" "{EXPERIMENT_FOLDER}"'
        )
        if result != 0:
            print(f"error copying experement result for {strategie}")
            continue
        os.system("rm *.exp")

        done.append(strategie)
        os.system(f'echo "{strategie}" >> {DONE_FILE}')


# benchmark="ALIA" # Done! (5)
# # benchmark="soundness" # Done! (6)
# # benchmark="AUFLIA AUFLIRA LIA UF UFIDL UFLIA UFLRA LRA" # Done! (7)
# # benchmark="QF_ALIA QF_AUFLIA QF_IDL QF_LIA QF_LRA QF_RDL QF_UF QF_UFIDL QF_UFLIA QF_UFLRA QF_LIRA" # Done! (8)
# # benchmark="LRA" # Done! (9)
# #benchmark="UF"
#
# prefix="ccfv-plus-042020"
# name="$prefix-$num.txt"
# comment="$num. $prefix run: very first ccfv plus benchmark"
#
#
# result="nancy_g5k:/home/hschurr/$name"
#
# mkdir $downloadfolder
#
# num="1-6"
# type=perf
# bin/GridTPT-launcher.py run -C "$cluster" -q -i nancy.g5k -w 00:59 -n $nodes -T "$type" -t 120 -b "$benchmark" -c "$comment" -O "$name"
#
# scp "$result" "$downloadfolder"
