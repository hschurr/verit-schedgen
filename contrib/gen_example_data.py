#!/usr/bin/env python3

# This script generates example data in a CSV file as used by the tutorial.

import csv
import random

with open("example_data.csv", "w") as example:
    writer = csv.DictWriter(
        example, ["benchmark", "logic", "strategy", "solved", "time"], delimiter=";"
    )
    writer.writeheader()
    base_names = list(map(lambda x: f"base{x:02}.smt2", range(1, 20 + 1)))
    for name in base_names:
        writer.writerow(
            {
                "benchmark": name,
                "logic": "UF",
                "strategy": "base-strategy",
                "solved": "yes",
                "time": f"{abs(random.gauss(0.5, 0.2)):.4}"
            }
        )
    writer.writerow(
        {
            "benchmark": "unsolved.smt2",
            "logic": "UF",
            "strategy": "base-strategy",
            "solved": "no",
            "time": f"{abs(random.gauss(0.5, 0.2)):.4}",
        }
    )

    for i in range(1, 7):
        print(i)
        strategy_name = f"extra{i:02}"
        for name in base_names:
            writer.writerow(
                {
                    "benchmark": name,
                    "logic": "UF",
                    "strategy": strategy_name,
                    "solved": "yes",
                    "time": f"{abs(random.gauss(1.5, 0.2)):.4}",
                }
            )
        for j in range(1, 7 - i):
            benchmark_name = f"extra{i:02}_{j:02}.smt2"
            writer.writerow(
                {
                    "benchmark": benchmark_name,
                    "logic": "UF",
                    "strategy": strategy_name,
                    "solved": "yes",
                    "time": f"{abs(random.gauss(0.5, 0.2)):.4}",
                }
            )
        writer.writerow(
            {
                "benchmark": "unsolved.smt2",
                "logic": "UF",
                "strategy": strategy_name,
                "solved": "no",
                "time": f"{abs(random.gauss(0.5, 0.2)):.4}",
            }
        )
    writer.writerow(
        {
            "benchmark": "special01.smt2",
            "logic": "UF",
            "strategy": "bad-strategy",
            "solved": "yes",
            "time": f"{abs(random.gauss(1.5, 0.2)):.4}",
        }
    )
